/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package sheridan;

import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author jaiminlakhani
 */
public class CartTest {
    
    public CartTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }


    @Test
    public void testGoodAddProduct() {
        System.out.println("Good Test of addProduct():");
        Product product1 = new Product("Jeans", 100);
        Cart cart1 = new Cart();
        cart1.addProduct(product1);
        assertEquals(1,cart1.getCartSize());
    }
    
    @Test
    public void testBadAddProduct() {
        System.out.println("Bad Test of addProduct()");
        Product product2 = new Product("Pant",10);
        Cart cart2 = new Cart();
        cart2.addProduct(product2);
        assertEquals(1,cart2.getCartSize());
    }
    
}
