/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sheridan;

/**
 *
 * @author jaiminlakhani
 */
public class DiscountFactory {
    private static DiscountFactory factory;
    
    private DiscountFactory() {}
    
    public static DiscountFactory getInstance() {
        if(factory == null)
            factory = new DiscountFactory();
        return factory;
    }
    
    public Discount getDiscount (DiscountTypes type) {
        switch(type) {
            case DISCOUNTBYAMOUNT : return new DiscountByAmount();
            case DISCOUNTBYPERCENTAGE : return new DiscountByPercentage();
        } 
        
        return null;
    }
}
