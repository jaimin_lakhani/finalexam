/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sheridan;

/**
 *
 * @author jaiminlakhani
 */
public enum DiscountTypes {
    DISCOUNTBYAMOUNT, DISCOUNTBYPERCENTAGE;
}
