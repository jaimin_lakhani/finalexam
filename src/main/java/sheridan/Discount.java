/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sheridan;

/**
 *
 * @author jaiminlakhani
 */
public abstract class Discount {
    public double amount;
    public abstract double calculateDiscount(double amount);
}
