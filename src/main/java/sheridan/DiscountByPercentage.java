/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sheridan;

/**
 *
 * @author jaiminlakhani
 */
public class DiscountByPercentage extends Discount{
    private double discount;
    private double discountPercentage = 10;
    public double calculateDiscount(double amount) {
        discount = amount * discountPercentage / 100;
        return discount;
    }
}
