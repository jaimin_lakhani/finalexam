/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sheridan;

/**
 *
 * @author jaiminlakhani
 */
public class DiscountByAmount extends Discount {  
    private double discount;
    private double discountDollarAmount = 50;
    public double calculateDiscount(double amount) {
        discount = discountDollarAmount;
        return discount;
    }  
}
